#!/bin/bash

set -e

export CURLOPT_FAILONERROR=1

export ROCKETCHAT_URL='https://chat.uviclaw.net'
export ROCKETCHAT_USER='topiarist'

source .secrets

# Authenticate

AUTH=$(curl --silent --show-error \
            --header "Content-Type: application/json" \
            ${ROCKETCHAT_URL}/api/v1/login \
            --data "{ \"user\": \"$ROCKETCHAT_USER\", \"password\": \"$ROCKETCHAT_PASSWORD\" }")

USER_ID=$(echo "$AUTH" | jq --raw-output '.data.userId'); export USER_ID
AUTH_TOKEN=$(echo "$AUTH" | jq --raw-output '.data.authToken')

# Prune

export ROCKETCHAT_ROOM_ID='GENERAL' # #lounge
# Time range starts at an arbitrary time before the server got started. Unix
# time zero would be more elegant but realistically indicative of an error
export PRUNE_OLDEST='2020-01-01T00:00:00Z'
PRUNE_LATEST=$(date --iso-8601=seconds --date '1 week ago'); export PRUNE_LATEST

curl --silent --show-error \
     --header "X-Auth-Token: $AUTH_TOKEN" \
     --header "X-User-Id: $USER_ID" \
     --header "Content-Type: application/json" \
     "${ROCKETCHAT_URL}/api/v1/rooms.cleanHistory" \
     --data "{ \"roomId\": \"$ROCKETCHAT_ROOM_ID\", \"oldest\": \"$PRUNE_OLDEST\", \"latest\": \"$PRUNE_LATEST\" }"

# Prune threads separately, due to a Rocket.Chat bug https://github.com/RocketChat/Rocket.Chat/issues/18738

source venv/bin/activate

AUTH_TOKEN=$AUTH_TOKEN python3 prune_threads.py $PRUNE_OLDEST $PRUNE_LATEST
