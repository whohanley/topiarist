import datetime
import dateutil.parser
import json
import os
import requests
import sys
import time

rocketchat_url = os.environ['ROCKETCHAT_URL']
room_id = os.environ['ROCKETCHAT_ROOM_ID']
user_id = os.environ['USER_ID']

prune_oldest = sys.argv[1]
prune_latest = sys.argv[2]

auth = {
    'X-User-Id': os.environ['USER_ID'],
    'X-Auth-Token': os.environ['AUTH_TOKEN']
}

threads = requests.get(f'{rocketchat_url}/api/v1/chat.getThreadsList?rid={room_id}', headers=auth)

def is_stale(message):
    message_time = dateutil.parser.isoparse(message['ts'])
    message_age = datetime.datetime.now(tz=datetime.timezone.utc) - message_time
    return message_age > datetime.timedelta(days=7)

for thread in json.loads(threads.text)['threads']:
    messages = requests.get(f'{rocketchat_url}/api/v1/chat.getThreadMessages?tmid={thread["_id"]}', headers=auth)
    for message in json.loads(messages.text)['messages']:
        if is_stale(message):
            requests.post(f'{rocketchat_url}/api/v1/chat.delete', headers=auth, data={'roomId': room_id, 'msgId': message['_id']})

    # Delete the thread itself
    if is_stale(thread):
        requests.post(f'{rocketchat_url}/api/v1/chat.delete', headers=auth, data={'roomId': room_id, 'msgId': thread['_id']})
